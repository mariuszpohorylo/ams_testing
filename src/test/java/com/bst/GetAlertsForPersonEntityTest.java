package com.bst;

import com.bst.commons.JsonManipulator;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.bst.ScreeningEndpoint.getNewAlertsId;

public class GetAlertsForPersonEntityTest {

    public static String alertsId;
    public static String payload;
    private static final String amsServiceBasePath = "https://ams-api.back.sparta.dev4.xara.ai";

    @BeforeAll
    public static void beforeAll() throws InterruptedException, IOException {
        ClassLoader classLoader = GetAlertsForPersonEntityTest.class.getClassLoader();
        File resourceFile = new File(classLoader.getResource("KongWahLauPersonEntityDetails.json").getFile());
        String payload = new String(Files.readAllBytes(Paths.get(resourceFile.getAbsolutePath())));

        String alertsId = getNewAlertsId(payload);
        ScreeningEndpoint.waitForAlertToBeGenerated(alertsId);
        GetAlertsForPersonEntityTest.alertsId = alertsId;
        GetAlertsForPersonEntityTest.payload = payload;
    }

    @DisplayName("Get alerts - Check response code")
    @Test
    public void testGetAlertsResponseCodeForPersonEntity() {
        Response response = RequestBuilder.getWithPathParameters(amsServiceBasePath + "/getAlert?request_id={id}", "id", alertsId);
        response.then().statusCode(200);
    }

    @DisplayName("Get alerts - Check response data")
    @Test
    public void testGetAlertsJsonResponseForPersonEntity() throws IOException {
        Response response = RequestBuilder.getWithPathParameters(amsServiceBasePath + "/getAlert?request_id={id}", "id", alertsId);
        JsonManipulator.saveResponseAsJsonFile(response,"./getAlertResponse.json");

        String responseString = JsonManipulator.getStringFromJsonFile("/getAlertResponse.json");
        String payloadString = JsonManipulator.getStringFromJsonFile("/src/test/resources/KongWahLauPersonEntityDetails.json");

        AlertDetails alertDetails = JsonManipulator.createJsonObject(payloadString, AlertDetails.class);
        AlertResponseDetails responseDetails =  JsonManipulator.createJsonObject(responseString, AlertResponseDetails.class);

        Assert.assertEquals(responseDetails.getEntityDetails().get(0).getEntityId(), alertDetails.getEntity().getEntityId());
        Assert.assertEquals(responseDetails.getEntityDetails().get(0).getEntityType(), alertDetails.getEntity().getEntityType());
        Assert.assertEquals(responseDetails.getEntityDetails().get(0).getName(), alertDetails.getEntity().getName());
    }
}