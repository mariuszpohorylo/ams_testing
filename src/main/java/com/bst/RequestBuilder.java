package com.bst;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

public class RequestBuilder {

    public static RequestSpecification Request;

    public static void createRequestSpec(){
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setContentType(ContentType.JSON);
        RequestSpecification requestSpec = builder.build();
        Request = RestAssured.given().spec(requestSpec);
    }

    public static Response get(String url) {
        createRequestSpec();
        return Request.get(url);
    }

    public static Response post(String url, String payload) {
        createRequestSpec();
        Request.body(payload);
        return Request.post(url);
    }

    public static Response getWithPathParameters(String url, String paramName, String paramValue) {
        createRequestSpec();
        Request.pathParam(paramName, paramValue);
        return Request.get(url);
    }

    public static Response postWithPathParameters(String url, String payload, Map<String, String> pathParams) {
        createRequestSpec();
        Request.pathParams(pathParams);
        Request.body(payload);
        return Request.post(url);
    }

}
