package com.bst;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class ScreeningEndpoint {

    private static final String screeningBasePath = "https://sparta-screening-api.front.sparta.dev4.xara.ai/v1";

    public static String getNewAlertsId(String payload){
        Response response = RequestBuilder.post(screeningBasePath + "/screen_rt", payload);
        return  response.then().contentType(ContentType.JSON).extract().path("id");
    }

    public static void waitForAlertToBeGenerated(String AlertsId) throws InterruptedException {
        String workflow_status = "Running";

        while(workflow_status.equals("Running")){

            Response response = RequestBuilder.getWithPathParameters( screeningBasePath + "/screen/{id}", "id", AlertsId);

            workflow_status = response.then().contentType(ContentType.JSON).extract().path("workflow_status");
            if(!workflow_status.equals("Running")){
                break;
            } else {
                Thread.sleep(1000);
            }
        }
    }
}
