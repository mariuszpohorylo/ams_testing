package com.bst;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class EntityDetails {

    private String name;

    @SerializedName(value = "entity_type")
    private String entityType;

    @SerializedName(value = "entity_id")
    private String entityId;
}
