package com.bst.commons;

import com.google.gson.Gson;
import io.restassured.response.Response;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JsonManipulator {

    public static String getStringFromJsonFile(String pathToFile) throws IOException {
        return new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") +  pathToFile)));
    }

    public static void saveResponseAsJsonFile(Response response, String fileName) throws IOException {
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(response.prettyPrint());
            file.flush();
        }
    }

    public static <T> T createJsonObject(String jsonString, Class<T> jsonObj){
        return  new Gson().fromJson(jsonString, jsonObj);
    }
}
