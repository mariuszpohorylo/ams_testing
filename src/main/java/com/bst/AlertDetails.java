package com.bst;

import lombok.Getter;

@Getter
public class AlertDetails {

    private String confidence;
    private EntityDetails entity;
}
