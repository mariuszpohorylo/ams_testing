package com.bst;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class AlertResponseDetails {

    @SerializedName(value = "results")
    private List<EntityDetails> entityDetails;
}
